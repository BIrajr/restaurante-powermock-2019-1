package br.ucsal.bes20191.testequalidade.restaurante.business;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20191.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20191.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.MesaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RestauranteBO.class })

public class RestauranteBOUnitarioTest {

	/**
	 * Metodo a ser testado: public static Integer abrirComanda(Integer
	 * numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException. Verificar
	 * se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * Lembre-se de verificar a chamada ao ComandaDao.incluir(comanda).
	 */
	@Before
	public void init (){
		PowerMockito.mock(ComandaDao.class);
		PowerMockito.mock(ItemDao.class);
		PowerMockito.mock(MesaDao.class);
	}
	
	
	
	@Test
	public void abrirComandaMesaLivre() throws Exception {
         RestauranteBO restaurante =  new RestauranteBO();
         Mesa mesa = new Mesa(1);
         Comanda comanda = new Comanda(mesa);
         RestauranteBO  spyRestaurante = PowerMockito.spy(restaurante);
		Integer numeroMesas = 2;
		
		
		
		
		PowerMockito.mockStatic(RestauranteBO.class);
		PowerMockito.when(RestauranteBO.class, "abrirComanda()",numeroMesas);
		PowerMockito.when(ComandaDao.class, "incluir()", comanda);
		
		PowerMockito.verifyStatic();
        ComandaDao.incluir(comanda);
		
		PowerMockito.verifyStatic();
		RestauranteBO.abrirComanda(numeroMesas);
		
		
		
	}
}
